<?php
include __DIR__ . '/MagicStick.php';

(new MagicStick())->cast(function ($x, $y){
    return $x * 2  === $y || $x * 2 + 1  === $y;
});