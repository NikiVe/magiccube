<?php
include __DIR__ . '/MagicStick.php';

(new MagicStick())->cast(function ($x, $y){
    return ($x - 1) * ($y - 1) === 0 || ($x === 0 && $y > 0) || ($y === 0 && $x > 0);
});