<?php
include __DIR__ . '/MagicStick.php';

(new MagicStick())->cast(function ($x, $y){
    return $x % 2 === 0 && $y % 3 === 0;
});