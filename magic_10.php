<?php
include __DIR__ . '/MagicStick.php';

(new MagicStick())->cast(function ($x, $y){
    return $y + 1 <= $x && $x - 1 <= $y * 2;
});