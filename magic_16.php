<?php
include __DIR__ . '/MagicStick.php';

(new MagicStick())->cast(function ($x, $y){
    return $y + $x > 14
        && $y - $x < 10
        && $x - $y < 10
        && $x + $y < 34;
});