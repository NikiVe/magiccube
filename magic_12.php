<?php
include __DIR__ . '/MagicStick.php';

(new MagicStick())->cast(function ($x, $y){
    return $x ** 2 + $y ** 2 < 20 ** 2 || ($x*$y === 0 && $x <= 20 && $y <= 20);
});