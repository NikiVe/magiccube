<?php
include __DIR__ . '/MagicStick.php';

(new MagicStick())->cast(function ($x, $y){
    return ($y % 6) === 0 || ($x % 6) === 0;
});