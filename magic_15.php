<?php
include __DIR__ . '/MagicStick.php';

(new MagicStick())->cast(function ($x, $y){
    return (($x - 9) - $y >= 0 && ($x - 9) - $y <= 11) ||
        (($x + 20) - $y >= 0 && ($x + 20) - $y <= 11);
});