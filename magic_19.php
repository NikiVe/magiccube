<?php
include __DIR__ . '/MagicStick.php';

(new MagicStick())->cast(function ($x, $y){
    return $x * $y === 0 || $x === 24 || $y === 24;
});