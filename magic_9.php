<?php
include __DIR__ . '/MagicStick.php';

(new MagicStick())->cast(function ($x, $y){
    return $y > $x + 10 || $y + 10 < $x;
});