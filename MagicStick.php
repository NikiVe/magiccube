<?php


class MagicStick
{
    public function cast(callable $spell) {
        for ($x = 0; $x < 25; $x++) {
            for ($y = 0; $y < 25; $y++) {
                echo $spell($x, $y) ?
                    "# " : ". ";
            }
            echo PHP_EOL;
        }
    }
}